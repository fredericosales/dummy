#!/bin/bash
#
#
clear; 
sudo apt update;
sudo apt upgrade -y;
sudo apt -f install;
sudo apt -y install \
    irssi \
    apache2 \
	libcanberra-gtk3-module \
	libcanberra-gtk-module \
	firmware-realtek \
	aptitude \
	gimp \
	inkscape \
	hunspell-pt-br \
	htop \
	nmap \
	p7zip-full \
	git \
    glances \
    ranger \
	numix-gtk-theme \
	numix-icon-theme \
	numix-icon-theme-circle \
	python-dev \
	python3-dev \
	virtualenv \
	virtualenvwrapper \
	virtualenv-clone \
	build-essential \
	cmake \
	bc \
	python3-pip \
	gparted \
	freecad \
	vim \
	vim-python-jedi \
	vim-gtk \
	ttf-mscorefonts-installer \
	texlive-full \
	gnome-shell-pomodoro \
	funcoeszz \
	wireshark \
	xterm \
	ufw \
    mtr \
	tshark \
	llvm \
	llvm-dev \
	tmux \
	firmware-linux-nonfree \
	firmware-linux-free \
	plymouth \
	plymouth-themes \
	wavemon \
	iptraf \
	iotop \
	bpython3 \
	texstudio \
	texstudio-l10n;

sudo apt -f install
