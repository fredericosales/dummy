#!/bin/bash
#
#
clear;
sudo apt update;
sudo apt -y upgrade;
sudo apt -y install libdbus-1-dev \
		    libusb-1.0-0-dev \
                    libsane-dev \
                    libsnmp-dev \
                    snmp \
                    libssl-dev \
                    python3-pyqt4 \
                    gtk2-engines-pixbuf \
                    python3-dev \
                    libtool \
                    libtool-bin \
                    xsane \
                    avahi-utils \
                    python3-notify2 \
                    python3-dbus.mainloop.qt;
