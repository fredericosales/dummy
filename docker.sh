#!/bin/bash
#
#
clear;
sudo apt install \
     apt-transport-https \
     ca-certificates \
     curl \
     gnupg2 \
     software-properties-common \
     -y;

curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -;
sudo apt-key fingerprint 0EBFCD88;
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/debian \
   $(lsb_release -cs) \
   stable";

sudo apt update;
sudo apt install docker-ce -y
sudo apt -f install;