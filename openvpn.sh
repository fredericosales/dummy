#!/bin/bash
#
#
clear;
echo "Installing OpenVPN Repo Keys and stuff..."
sudo apt update && sudo apt -y install ca-certificates wget net-tools gnupg
wget -qO - https://as-repository.openvpn.net/as-repo-public.gpg | sudo apt-key add -
sudo echo "deb http://as-repository.openvpn.net/as/debian buster main" | sudo tee /etc/apt/sources.list.d/openvpn-as-repo.list
sudo apt update && sudo apt -y install openvpn-as
echo "";
echo "done."
