#!/bin/bash
#
# Frederico Sales
# <frederico.sales@engenharia.ufjf.br>
# 2019
#

clear;
wget -O - https://debian.neo4j.org/neotechnology.gpg.key | sudo apt-key add -;
echo 'deb https://debian.neo4j.org/repo stable/' | sudo tee -a /etc/apt/sources.list.d/neo4j.list;
sudo apt update;
sudo apt install neo4j -y
sudo apt -f install
